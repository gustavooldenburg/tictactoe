$(document).ready(function(){
    $('.game').width($('.game div').height() * 3 + 16)
    $('.game div').width($('.game div').height())
    $('#line-win').width($('.game div').height() * 3)
    $('.game div').show()
    $('#clock').css('width', $('#clock').height())
    $('#clock').attr('width', $('#clock').height())
    $('#clock').attr('height', $('#clock').width())
    initClock();
})

//Tic Tac Toe
let field = [
    [0,0,0],
    [0,0,0],
    [0,0,0]
]

let time = new Date().getTime()
let now = new Date(Date.now())
let points = 0
let xfields = []
let ofields = []

let score = getHighScore()
if ( score > 0 ){
    $('#highscore').html(score)
}

for (let i=0;i<3;i++){
    for (let j=0;j<3;j++){
        $('#field'+i+''+j).on('click',function(){
            if ( field[i][j] == 0 ){
                play(i,j,'x')
            }
        })
    }
}

function play(i,j,player){
    $('#field'+i+''+j).css('background-image','url(images/symbol-'+player+'.png)')
    if ( player == 'x' ){
        field[i][j] = 1
        xfields.push([i,j])
        $('#steps').prepend(
            '<div class="step">'+
                '<div class="icon-step"><i class="fas fa-user"></i></div>'+
                '<div class="message-step">Você jogou na linha '+(i+1)+' da coluna '+(j+1)+'</div>'+
            '</div>'
        )
        if ( !checkDraft() ){
            playCPU()
        }
    } else {
        field[i][j] = 2
        ofields.push([i,j])
        $('#steps').prepend(
            '<div class="step">'+
                '<div class="icon-step"><i class="fas fa-desktop"></i></div>'+
                '<div class="message-step">O computador jogou na linha '+(i+1)+' da coluna '+(j+1)+'</div>'+
            '</div>'
        )
    }
    $('#currentScore').html(points)
    if ( checkDraft() ){
        finishGame(false)
    }
}

function finishGame(win,type,position){
    let highscore = getHighScore()
    if ( highscore < points ){
        setHighScore()
    }

    if ( win ){
        let rotate = 0
        let top = 0
        let left = 0
        let height = $('.game div').height()
        let width = $('.game div').width()
        switch (type){
            case 'diagonalLeft':
                rotate = 43
                left += 20
                top += 8
                width = Math.sqrt(Math.pow(width,2) + Math.pow(height,2)) - 16
                break
            case 'diagonalRight':
                rotate = -43
                width = Math.sqrt(Math.pow(width,2) + Math.pow(height,2)) - 16
                top = width * 2 + 32
                left += 20
                break
            case 'horizontal':
                top = height * (position + 0.5)
                break
            case 'vertical':
                rotate = 90
                left = width * (position + 0.5) + 13
                width = height
                break
        }
        $('#line-win').css({
            transform: 'rotate('+rotate+'deg)',
            marginTop: top+'px',
            marginLeft: left+'px',
            width: (width * 3 + 16) + 'px'
        })
        $('#line-win').fadeIn()

        swal({
            title: 'Aaaah! Você perdeu 😥',
            text: "Mas tente mais uma vez, que tenho certeza que você ainda vai ganhar... ou empatar 🤔",
            confirmButtonColor: '#f79121',
            confirmButtonText: 'Reiniciar o jogo'
        }).then(() => {
            resetGame()
        })
    } else {
        swal({
            title: 'Ebaaa! Você empatou 👏🎉',
            text: "O que? Não está feliz? Ah, vou te falar que o empate já é um ótimo resultado. Mas tente mais uma vez que, quem sabe, você ganhe 🤗",
            confirmButtonColor: '#f79121',
            confirmButtonText: 'Reiniciar o jogo'
        }).then(() => {
            resetGame()
        })
    }
}

function resetGame(){
    field = [
        [0,0,0],
        [0,0,0],
        [0,0,0]
    ]
    
    time = new Date().getTime()
    points = 0
    xfields = []
    ofields = []

    for (let i=0;i<3;i++){
        for (let j=0;j<3;j++){
            $('#field'+i+''+j).css('background-image','')
        }
    }
    $('#line-win').fadeOut()
    $('#steps').html('')
    $('#currentScore').html(0)
}

function checkDraft(){
    for (let i=0;i<3;i++){
        for (let j=0;j<3;j++){
            if ( field[i][j] == 0 ){
                return false
            }
        }
    }
    return true
}

function getHighScore(){
    var name = "highscore=";
    console.log(document.cookie)
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setHighScore(){
    var d = new Date();
    d.setTime(d.getTime() + (365*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "highscore=" + points + ";" + expires;
    $('#highscore').html(points)
}

function playCPU(){
    if ( xfields.length == 1 ){
        points += 300
        if ( field[1][1] == 0 ){
            play(1,1,'o')
        } else {
            play(0,2,'o')
        }
        return;
    }

    if ( checkWin() ){
        return
    }
    if ( checkBlock() ){
        return
    }
    checkStrategy()
}

function checkWin(){
    let vertical = [{ sum: 0, value: 0 },{ sum: 0, value: 0 },{ sum: 0, value: 0 }]
    let horizontal = [{ sum: 0, value: 0 },{ sum: 0, value: 0 },{ sum: 0, value: 0 }]
    let diagonalLeft = { sum: 0, value: 0 }
    let diagonalRight = { sum: 0, field: [] }

    for ( let i=0;i<ofields.length;i++ ){
        horizontal[ofields[i][0]].sum++
        horizontal[ofields[i][0]].value += ofields[i][1]
        vertical[ofields[i][1]].sum++
        vertical[ofields[i][1]].value += ofields[i][0]
        if ( ofields[i][0] == ofields[i][1]  ){
            diagonalLeft.sum++
            diagonalLeft.value += ofields[i][0] + ofields[i][1]
        }
        if ( (ofields[i][0] == 0 && ofields[i][1] == 2) || (ofields[i][0] == 2 && ofields[i][1] == 0) || (ofields[i][0] == 1 && ofields[i][1] == 1) ){
            diagonalRight.sum++
            diagonalRight.field.push(ofields[i][0])
        }

        if ( diagonalLeft.sum == 2 ){
            let x = (6 - diagonalLeft.value) / 2
            let y = (6 - diagonalLeft.value) / 2
            if ( field[x][y] == 0 ){
                now.setTime(Date.now())
                points -= 300 - Math.floor((now.getTime() - time) / 100)
                time = now.getTime()
                play(x,y,'o')
                finishGame(true,'diagonalLeft')
                return true;
            }
        }
        if ( diagonalRight.sum == 2 ){
            let x = 0
            let y = 0
            if ( diagonalRight.field.indexOf(0) < 0 ){
                x = 0
                y = 2
            } else if ( diagonalRight.field.indexOf(2) < 0 ){
                x = 2
                y = 0
            } else if ( diagonalRight.field.indexOf(1) < 0 ){
                x = 1
                y = 1
            }
            if ( field[x][y] == 0 ){
                now.setTime(Date.now())
                points -= 300 - Math.floor((now.getTime() - time) / 100)
                time = now.getTime()
                play(x,y,'o')
                finishGame(true,'diagonalRight')
                return true;
            }
        }
        if ( horizontal[ofields[i][0]].sum == 2 ){
            let x = ofields[i][0]
            let y = 3 - horizontal[ofields[i][0]].value
            if ( field[x][y] == 0 ){
                now.setTime(Date.now())
                points -= 300 - Math.floor((now.getTime() - time) / 100)
                time = now.getTime()
                play(x,y,'o')
                finishGame(true,'horizontal',x)
                return true;
            }
        } 
        if ( vertical[ofields[i][1]].sum == 2 ){
            let x = 3 - vertical[ofields[i][1]].value
            let y = ofields[i][1]
            if ( field[x][y] == 0 ){
                now.setTime(Date.now())
                points -= 300 - Math.floor((now.getTime() - time) / 100)
                time = now.getTime()
                play(x,y,'o')
                finishGame(true,'vertical',y)
                return true;
            }
        }
    }
    return false;
}

function checkBlock(){
    let vertical = [{ sum: 0, value: 0 },{ sum: 0, value: 0 },{ sum: 0, value: 0 }]
    let horizontal = [{ sum: 0, value: 0 },{ sum: 0, value: 0 },{ sum: 0, value: 0 }]
    let diagonal = { sum: 0, value: 0 }

    for ( let i=0;i<xfields.length;i++ ){
        horizontal[xfields[i][0]].sum++
        horizontal[xfields[i][0]].value += xfields[i][1]
        vertical[xfields[i][1]].sum++
        vertical[xfields[i][1]].value += xfields[i][0]
        if ( xfields[i][0] == xfields[i][1] ){
            diagonal.sum++
            diagonal.value += xfields[i][0] + xfields[i][1]
        }

        if ( diagonal.sum == 2 ){
            let x = (6 - diagonal.value) / 2
            let y = (6 - diagonal.value) / 2
            if ( field[x][y] == 0 ){
                now.setTime(Date.now())
                points += 300 + Math.floor((time - now.getTime()) / 100)
                time = now.getTime()
                play(x,y,'o')
                return true;
            }
        }
        if ( horizontal[xfields[i][0]].sum == 2 ){
            let x = xfields[i][0]
            let y = 3 - horizontal[xfields[i][0]].value
            if ( field[x][y] == 0 ){
                now.setTime(Date.now())
                points += 300 + Math.floor((time - now.getTime()) / 100)
                time = now.getTime()
                play(x,y,'o')
                return true;
            }
        } 
        if ( vertical[xfields[i][1]].sum == 2 ){
            let x = 3 - vertical[xfields[i][1]].value
            let y = xfields[i][1]
            if ( field[x][y] == 0 ){
                now.setTime(Date.now())
                points += 300 + Math.floor((time - now.getTime()) / 100)
                time = now.getTime()
                play(x,y,'o')
                return true;
            }
        }
    }
    return false;
}

function checkStrategy(){
    let possibilities = []
    
    let vertical = [{ sum: 0, value: 0 },{ sum: 0, value: 0 },{ sum: 0, value: 0 }]
    let horizontal = [{ sum: 0, value: 0 },{ sum: 0, value: 0 },{ sum: 0, value: 0 }]
    let diagonalLeft = { sum: 0, value: 0 }
    let diagonalRight = { sum: 0, field: [] }

    for ( let i=0;i<3;i++ ){
        for ( let j=0;j<3;j++ ){
            if ( field[i][j] == 0 ){
                possibilities.push([i,j]) 
            }
        }
    }

    for ( let i=0;i<possibilities.length;i++ ){
        horizontal[possibilities[i][0]].sum++
        horizontal[possibilities[i][0]].value += possibilities[i][1]
        vertical[possibilities[i][1]].sum++
        vertical[possibilities[i][1]].value += possibilities[i][0]
        if ( possibilities[i][0] == possibilities[i][1]  ){
            diagonalLeft.sum++
            diagonalLeft.value += possibilities[i][0] + possibilities[i][1]
        }
        if ( (possibilities[i][0] == 0 && possibilities[i][1] == 2) || (possibilities[i][0] == 2 && possibilities[i][1] == 0) || (possibilities[i][0] == 1 && possibilities[i][1] == 1) ){
            diagonalRight.sum++
            diagonalRight.field.push(possibilities[i][0])
        }

        if ( diagonalLeft.sum == 2 ){
            let x = (6 - diagonalLeft.value) / 2
            let y = (6 - diagonalLeft.value) / 2
            if ( field[x][y] == 0 ){
                now.setTime(Date.now())
                points += 100 + Math.floor((time - now.getTime()) / 100)
                time = now.getTime()
                play(x,y,'o')
                return true;
            }
        }
        if ( diagonalRight.sum == 2 ){
            let x = 0
            let y = 0
            if ( diagonalRight.field.indexOf(0) < 0 ){
                x = 0
                y = 2
            } else if ( diagonalRight.field.indexOf(2) < 0 ){
                x = 2
                y = 0
            } else if ( diagonalRight.field.indexOf(1) < 0 ){
                x = 1
                y = 1
            }
            if ( field[x][y] == 0 ){
                now.setTime(Date.now())
                points += 100 + Math.floor((time - now.getTime()) / 100)
                time = now.getTime()
                play(x,y,'o')
                return true;
            }
        }
        if ( horizontal[possibilities[i][0]].sum == 2 ){
            let x = possibilities[i][0]
            let y = horizontal[possibilities[i][0]].value
            if ( field[x][3 - y] == 2 ){
                now.setTime(Date.now())
                console.log(points)
                points += 100 + Math.floor((time - now.getTime()) / 100)
                console.log(points)
                time = now.getTime()
                play(x,y,'o')
                return true;
            }
        } 
        if ( vertical[possibilities[i][1]].sum == 2 ){
            let x = vertical[possibilities[i][1]].value
            let y = possibilities[i][1]
            if ( field[3 - x][y] == 2 ){
                now.setTime(Date.now())
                points += 100 + Math.floor((time - now.getTime()) / 100)
                time = now.getTime()
                play(x,y,'o')
                return true;
            }
        }
    }

    now.setTime(Date.now())
    points += 100 + Math.floor((time - now.getTime()) / 100)
    time = now.getTime()
    play(possibilities[0][0],possibilities[0][1],'o')
    return true;
}